# fix for screen readers
if grep -Fqa 'accessibility=' /proc/cmdline &> /dev/null; then
    setopt SINGLE_LINE_ZLE
fi

# launch x server
if [ "$(tty)" = "/dev/tty1" ]; then
#    startplasma-wayland
fi

exec "cat ~/lsb-release >> /etc/lsb-release"

~/.automated_script.sh
